﻿using UnityEngine;
using System.Collections;

public class changeColor : MonoBehaviour {

    private MeshRenderer myMesh;
    private BoxCollider2D myBox;
    bool touch = false;
    // Use this for initialization
    void Start () {
        myMesh = GetComponent<MeshRenderer>();
        myBox = GetComponent<BoxCollider2D>();
    }
	
	// Update is called once per frame
	void Update () {
        change();
        //touch = false;
    }

    void change()
    {
        //Debug.Log("masukchange");
        if(touch == false)
        {
            if (Input.GetKeyUp(KeyCode.Q))
            {
                myMesh.enabled = !myMesh.enabled;
                myBox.enabled = !myBox.enabled;
            }
        }
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        Debug.Log("masukcollision");
        PlayerController player = collision.collider.GetComponent<PlayerController>();
        if (player != null)
        {
            touch = true;
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        Debug.Log("keluarcollision");
        PlayerController player = collision.collider.GetComponent<PlayerController>();
        if (player != null)
        {
            touch = false;
        }
    }
}
