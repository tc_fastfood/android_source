package com.example.utomo.database;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kosalgeek.asynctask.AsyncResponse;
import com.kosalgeek.asynctask.PostResponseAsyncTask;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements AsyncResponse{

    EditText etUsername, etPassword;
    Button btnLogin, btndaftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etUsername = (EditText) findViewById(R.id.email);
        etPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.button);
        btndaftar = (Button) findViewById(R.id.daftar);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap postData = new HashMap();
                postData.put("mobile", "android");
                postData.put("txtEmail", etUsername.getText().toString());
                postData.put("txtPassword", etPassword.getText().toString() );

                PostResponseAsyncTask loginTask =
                        new PostResponseAsyncTask(MainActivity.this, postData);
                loginTask.execute("http://10.0.2.2/client/login.php");
            }
        });
        btndaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, Daftar.class);
                startActivity(in);
            }
        });
    }

    @Override
    public void processFinish(String result) {
        if(result.equals("success")){
            System.out.println("email "+ etUsername.getText().toString());
            Toast.makeText(this, "Login Succesful", Toast.LENGTH_LONG).show();

            datauser.getInstance().setEmail(etUsername.getText().toString());
            datauser.getInstance().setPassword(etPassword.getText().toString());

            Intent in = new Intent(this, Listuser.class);
            startActivity(in);
        }
        else{
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }
}
