package com.example.utomo.database;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kosalgeek.asynctask.AsyncResponse;
import com.kosalgeek.asynctask.PostResponseAsyncTask;

import java.util.HashMap;

public class Daftar extends AppCompatActivity implements AsyncResponse {

    EditText etNama, etPassword, etEmail, etNohp, etkonpass;
    Button btnDaftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);

        etNama = (EditText) findViewById(R.id.nama);
        etNohp = (EditText) findViewById(R.id.no_hp);
        etEmail = (EditText) findViewById(R.id.email);
        etPassword = (EditText) findViewById(R.id.password);
        etkonpass = (EditText) findViewById(R.id.konpassword);
        btnDaftar = (Button) findViewById(R.id.btnDaftar);
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etPassword.getText().toString().equals(etkonpass.getText().toString())){
                    HashMap postData = new HashMap();
                    postData.put("mobile", "android");
                    postData.put("txtName", etNama.getText().toString());
                    postData.put("txtNohp", etNohp.getText().toString());
                    postData.put("txtEmail", etEmail.getText().toString());
                    postData.put("txtPass", etPassword.getText().toString());

                    PostResponseAsyncTask daftarTask =
                            new PostResponseAsyncTask(Daftar.this, postData);
                    daftarTask.execute("http://10.0.2.2/client/Daftar.php");
                }
                else{
                    Toast.makeText(getApplicationContext(), "Cek password anda", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void processFinish(String result) {
        if(result.equals("success")){
            Toast.makeText(this, "Daftar Succesful", Toast.LENGTH_LONG).show();
            Intent in = new Intent(this, MainActivity.class);
            startActivity(in);
        }
        else{
            Toast.makeText(this, "Daftar Failed", Toast.LENGTH_LONG).show();
        }
    }
}
