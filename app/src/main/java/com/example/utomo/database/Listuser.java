package com.example.utomo.database;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.amigold.fundapter.BindDictionary;
import com.amigold.fundapter.FunDapter;
import com.amigold.fundapter.extractors.StringExtractor;
import com.kosalgeek.android.json.JsonConverter;
import com.kosalgeek.asynctask.AsyncResponse;
import com.kosalgeek.asynctask.PostResponseAsyncTask;

import java.util.ArrayList;

public class Listuser extends AppCompatActivity {

    final String LOG = "listuser";
    private ArrayList<user> userList;
    private ListView lvUser;
    TextView menuPemesanan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listuser);

        menuPemesanan = (TextView) findViewById(R.id.labelPesanan);
        //menghubungkan php dengan android
        PostResponseAsyncTask taskread = new PostResponseAsyncTask(Listuser.this, new AsyncResponse() {
            @Override
            public void processFinish(String s) {
                //Log.d(LOG, s);
                userList = new JsonConverter<user>().toArrayList(s, user.class);

                //masukin string dari kelas lain kesini
                BindDictionary<user> dict = new BindDictionary<user>();
                dict.addStringField(R.id.profilName, new StringExtractor<user>() {
                    @Override
                    public String getStringValue(user item, int position) {
                        return item.Nama;
                    }
                });

                FunDapter<user> adapter = new FunDapter<>(Listuser.this, userList, R.layout.layout_listprofil, dict);

                lvUser = (ListView)findViewById(R.id.lvUser);
                lvUser.setAdapter(adapter);

            }
        });

        taskread.execute("http://10.0.2.2/client/listprofil.php");

        menuPemesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Listuser.this, listPemesan.class);
                startActivity(in);
            }
        });

    }
}
