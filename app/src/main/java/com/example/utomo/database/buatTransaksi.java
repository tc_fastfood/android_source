package com.example.utomo.database;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class buatTransaksi extends AppCompatActivity {

    final String LOG = "listuser";
    TextView menuProfil, menuPesanan, namaPemesan;
    EditText namapemesan;
    ImageButton btnpost;
    String id_Member;
    private ArrayList<user> namaUser;
    String email,password,nama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat_transaksi);

        menuProfil = (TextView) findViewById(R.id.labelProfil);
        menuPesanan = (TextView) findViewById(R.id.labelPesanan);
        btnpost = (ImageButton) findViewById(R.id.btnPost);
        namapemesan = (EditText) findViewById(R.id.namaPemesan);

        email = datauser.getInstance().getEmail();
        password = datauser.getInstance().getPassword();

        System.out.println("Email & Pass "+ email +" " + password);

        HashMap postData = new HashMap();
        postData.put("txtEmail", email);
        postData.put("txtPassword", password);

        PostResponseAsyncTask taskread = new PostResponseAsyncTask(buatTransaksi.this, postData , new AsyncResponse() {
            @Override
            public void processFinish(String s) {
                Log.d(LOG, s);

                try{
                    JSONArray jArray = new JSONArray(s);
                    JSONObject jObject = jArray.getJSONObject(0);

                    nama = jObject.optString("Nama").toString();
                    id_Member = jObject.optString("ID_Member").toString();
                }

                catch (JSONException e) {e.printStackTrace();}
                System.out.println("Nama: "+nama);
                System.out.println("Id: "+id_Member);
                namapemesan.setText(nama);
            }
        });

        taskread.execute("http://10.0.2.2/client/nama.php");

        btnpost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(buatTransaksi.this, Listuser.class);
                startActivity(in);
            }
        });

        menuProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(buatTransaksi.this, Listuser.class);
                startActivity(in);
            }
        });

        menuPesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(buatTransaksi.this, listPemesan.class);
                startActivity(in);
            }
        });
    }
}
