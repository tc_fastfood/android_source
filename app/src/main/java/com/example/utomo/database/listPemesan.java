package com.example.utomo.database;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class listPemesan extends AppCompatActivity {

    TextView menuProfil;
    ImageButton btnBuatpemesanan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_pemesan);

        menuProfil = (TextView) findViewById(R.id.labelProfil);
        btnBuatpemesanan = (ImageButton) findViewById(R.id.btnBuatpemesanan);

        menuProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(listPemesan.this, Listuser.class);
                startActivity(in);
            }
        });

        btnBuatpemesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent buatPemesaan = new Intent(listPemesan.this, buatTransaksi.class);
                startActivity(buatPemesaan);
            }
        });
    }
}
