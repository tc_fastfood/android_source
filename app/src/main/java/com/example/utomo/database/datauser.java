package com.example.utomo.database;

/**
 * Created by utomo on 5/3/2016.
 */
public class datauser {
    private String email,password;

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    private static final datauser holder = new datauser();
    public static datauser getInstance() {return holder;}
}
